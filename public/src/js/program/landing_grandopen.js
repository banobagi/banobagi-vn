var mainSlider = $(".js-main-slider");

mainSlider.on('init reInit beforeChange', function(event, slick, currentSlide, nextSlide){
    var i = (nextSlide ? nextSlide : 0) + 1;
    $(".cont-slider__page").text(i + '/' + slick.slideCount);
});

mainSlider.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    fade: true,
    arrow: true,
    asNavFor: '.js-thum-slider',
    dot: true,
    prevArrow: ".js-slider-prev",
    nextArrow: ".js-slider-next",
});

$('.js-thum-slider').slick({
    asNavFor: '.js-main-slider',
    focusOnSelect: true,
    centerMode: true,
    slidesToShow: 5,
    arrows: false,
    responsive: [
       {
            breakpoint: 1025,
            settings: {
                slidesToShow: 3
            }
       }
   ]
});

/**
* Youtube API 로드
*/
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

/**
* onYouTubeIframeAPIReady 함수는 필수로 구현해야 한다.
* 플레이어 API에 대한 JavaScript 다운로드 완료 시 API가 이 함수 호출한다.
* 페이지 로드 시 표시할 플레이어 개체를 만들어야 한다.
*/
var player;
function onYouTubeIframeAPIReady(){
    player = new YT.Player('program-iframe', {
        height: '360',
        width: '640',
        videoId: 'vkvIQRBDGs0',
        events: {
            'onReady': onPlayerReady,               // 플레이어 로드가 완료되고 API 호출을 받을 준비가 될 때마다 실행
            'onStateChange': onPlayerStateChange    // 플레이어의 상태가 변경될 때마다 실행
        }
    });
}

function onPlayerReady(event) {
// 플레이어 자동실행 (주의: 모바일에서는 자동실행되지 않음)
//            event.target.playVideo();
}

var playerState;

function onPlayerStateChange(event) {
playerState = event.data == YT.PlayerState.ENDED ? '종료됨' :
                event.data == YT.PlayerState.PLAYING ? '재생 중' :
                event.data == YT.PlayerState.PAUSED ? '일시중지 됨' :
                event.data == YT.PlayerState.BUFFERING ? '버퍼링 중' :
                event.data == YT.PlayerState.CUED ? '재생준비 완료됨' :
                event.data == -1 ? '시작되지 않음' : '예외';

    // 재생여부를 통계로 쌓는다.
    collectPlayCount(event.data);

    if (event.data === 0) {
        // 종료 후 작업을 여기에 코딩
        $(".program-video").show()
    }
}

function playYoutube() {
    // 플레이어 자동실행 (주의: 모바일에서는 자동실행되지 않음)
    player.playVideo();
}

function pauseYoutube() {
    player.pauseVideo();
}

function stopYoutube() {
    player.seekTo(0, true);     // 영상의 시간을 0초로 이동시킨다.
    player.stopVideo();
}

var played = false;
function collectPlayCount(data) {
    if (data == YT.PlayerState.PLAYING && played == false) {
        // todo statistics
        played = true;
    }
}

$('.program-video__btn').on('click',function(e){
    e.preventDefault();
    playYoutube();
    $('.program-video').hide();
})
